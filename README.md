# bloxberg-pipeline-template
A template for Gitlab ci/cd pipelines including:
- deploying to: 
	- staging
	- qa
	- production
- automated tests: 
	- unit tests
	- integration tests
	- end to end tests

# Documentation
Documentation for different topics can be found here:
- [whitepaper automated tests](https://keeper.mpdl.mpg.de/smart-link/201e33c6-4f8d-473f-9b04-d46fc096bf34/)
- [using template for a project](./documentation.md)
