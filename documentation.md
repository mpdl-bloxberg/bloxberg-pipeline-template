# Table of content

1. [Installing the pipeline in the project](#enable-in-project)
1. [Using and configuring the pipeline](#usage)
1. [Using the automated test framework](#automated-tests)
1. [Troubleshooting](#troubleshooting)

# Installation

To install the pipeline you need to 
1. [enable in project](#enable-in-project)
1. [add pipeline submodule](#add-submodule)
1. [manage kubernetes permissions](#permissions)

### Enable in Project
Enable pipeline features for a project in the settings:

- enable `general->visibility,project-features,permissions->container-registry`
- disable `ci/cd->runners->shared-runners`
- disable `ci/cd->general-pipelines->skip-outdated-deployment-jobs`
- create deploy-token `repository->deploy-tokens`
	- name is `gitlab-deploy-token`
	- permissions are `read_registry` and `write_registry`

### Add submodule
Add this repository as submodule to the project:
```
# in project root
git submodule add https://gitlab.mpcdf.mpg.de/mpdl-bloxberg/bloxberg-pipeline-template.git
```
For nested projects use [supergit](https://gitlab.mpcdf.mpg.de/mpdl-bloxberg/bloxberg-pipeline-template.git)
Then copy files into the project root
```
cp bloxberg-pipeline-template/.autodevops-template.yml .
cp bloxberg-pipeline-template/.gitlab-ci.yml .
```

### Permissions
After performing those steps and pushing, the pipeline will fail with permission
issues. 
Contact me with the error message and I will add the permissions needed. 



# Usage
Now that the pipeline is installed it needs to be configured to the project by
changing values in `.gitlab-ci.yml`: 

- [set the project name](#project-name)
- [define persistent volumes](#persistent-volumes)
- [define secrets ](#secrets)
- [define environment variables ](#environment-variables)
- [define arguments](#arguments)
- [expose ports](#ports)
- [set resource limits](#limits)

#### Project name
First you will need to set the project name.
Set it so it explains what it is doing, i.e.: `PROJECT_NAME: bootnode`.
All configurations are done in the `Variables:` section of `.gitlab-ci.yml`

#### Persistent volumes
To use persistent volumes change the variable `PERSISTENT_VOLUMES`
List the volumes (space separated) with the following syntax: 
```
	Variables:
		PERSISTENT_VOLUMES: "volumename:mountpath:size [...]"`
```

- `volumename` is not relevant, as long as you don't use the same twice in the same project. 
- `mountpath` is where the volume will be mounted in the Container.
- `size` needs an unit i.e. `10Gi`. 

#### Secrets
Secrets are similar to persistent volumes.
They are defined in `SECRETS`. 
List the secrets (again space separated) with the following syntax
```
	Variables: 
		SECRETS: "secretname:mountpath [...]"
```

- `mountpath` is where the secret will be mounted in the Container.
- The name needs to match with a kubernetes secret, which you can create with: 
	kubectl create secret generic <secret-name> -n <namespace> --from-file <file> [--from-file <another-file>]

#### Environment variables
If you need to set environment variables you can set them in different stages or servers: 
Prefix the variables with the name you scope you need,
i.e. if you want to set `IP` for staging, set `ENV_STAGING_IP` in the pipeline file.

There are 6 prefixes:
- `ENV_STAGING_` for staging
- `ENV_QA_` for qa
- `ENV_PROD_` for production
- `ENV_NUT_` for nut
- `ENV_THOTH_` for thoth
- `ENV_ALL_` for all

> `ALL` is set first and overwritten by `STAGING/QA/PROD` which is overwritten by `THOTH/NUT`

Note that react sets the environment variables at runtime.
To use the described feature, an additional workaround, documented [here](./tmplt/react/react.md), is needed

#### Arguments
Arguments it the only thing you cannot define in `.gitlab-ci.yml` yet. 
For now set them in the list `args` in `.pipeline/qa/k8s/deployment.yaml` 

#### Ports
If you want to expose ports,
list in `INTERNAL_EXPOSE` (space separation) and `EXTERNAL_EXPOSE`
```
	Variables: 
		INTERNAL_EXPOSE: "8545 8546"
		EXTERNAL_EXPOSE: "30303"
```

#### Limits
you can set limits for the deployment 
```
	Variables: 
		MAX_MEM: "4e9"
		MAX_CPU: "1"
```
More info on limits can be found on
[kubernetes](https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/cpu-default-namespace/)

### Liveness Command
Kubernetes checks if created containers are running, this is done with http/tcp
requests.
If responds to neither requests you need to specify a command
```
	Variables: 
		LIVENESS_COMMAND: "psgrep ..."
```

The timeout and the initial delay, before starting liveness checks, can be
changed with:
```
   Variables:
      LIVENESS_PROBE_INITIAL_DELAY: 900
      LIVENESS_PROBE_TIMEOUT: 30
```

#### Hostname
you can specify an address under which the service is available from outside wit
```
   Variables:
      ADDITIONAL_HOSTS: 'blockscout.bloxberg.org'
```

you can also restrict the ip addresses that are allowed to access this site
with:

```
   Variables:
      WHITELIST: "127.0.0.1,127.0.0.2"
```


### repository file structure
For the pipeline you'll need a Dockerfile in the repository root.


# Automated tests
The pipeline runs automated tests: 

- unit tests: run in staging(isolated from other services)
- integration tests: run in qa before program is started(like unit tests but not isolated) 
- end to end tests: run in qa after program is started

The pipeline looks for test scripts:
```
$PROJECT_ROOT/tests/unit-test.sh
$PROJECT_ROOT/tests/integration-test.sh
$PROJECT_ROOT/tests/end-to-end-test.py
```

There are templates to guide you create those tests for: 

- Java script
- React

### JavaScript
To install this template:

- `cp -r $PROJECT_ROOT/bloxberg-pipelie-template/templates/js/test tests`
- Adjust `package.json` file (`package.json.example` for inspiration)
	- `scripts`: for tests
	- `devDependencies`: packages needed for testing (`jest`, ...)
- Install required packages in the project `Dockerfile`
	- `npm` and `nodejs` for unit and integration tests

Create test cases: inspiration from: 

- `tests/unit-test/*`
- `tests/end-to-end-test/*`
- `tests/integration-test/*`

### React
React apps build process.env at compile time, which makes it impossible to use
the same compiled version for qa and production, without using this workarounds: 

1. Add the script `./env-to-json.sh` to the Docker image
2. Call the script in `start.sh`
	```
	./env-to-json.sh
	```
3. In index.js fetch the file with the current environment: 

	```
	fetch("./env.json")
		.then(response => response.json())
		.then(json => {
			window.env = json;

			// main function here
		});
	```
4. Add the variables you need to the pipeline.
	If you need `VARIABLE=1` in qa and `VARIABLE=2` in prod set `.gitlab-ci.yaml`: 

	```
	ENV_QA_REACT_APP_VARIABLE:1
	ENV_PROD_REACT_APP_VARIABLE: 2
	```


# Troubleshooting
## Job stuck
Jobs will only work on runners that are tagged. The tags are not persistent
however, and since nut frequently reboots and looses its tag, manually adding
the tag back is necessary.

This is done under: `Groups->CI/CD->Runners`
and add `nut` to the runner that is not tagged
