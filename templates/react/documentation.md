# React Runtime environment variable

In CI/CD pipeline making good use of environment variable can cut down resource
usage and reduce errors.
Say for example an application needs to connect to a test network in staging and
to another in production.

Without runtime variables one would have to compile two almost identical
applications. One with `URL=123` and the other with `URL=456`. 
With runtime variables the applications are identical `URL=env.URL`, but they
run in different environments


React apps however build process.env at compile time, which makes it impossible

There is a workaround to get it working in 3 steps:
1. Add the script `./env-to-json.sh` to the Docker image
3. Call the script in `start.sh`
	```
	./env-to-json.sh
	```
2. In JS fetch the file with the current environment: 
	```
	async function get_env(){
		response = await fetch("/env.json");
		window.env = await response.json();
	}
	get_env();
	```
