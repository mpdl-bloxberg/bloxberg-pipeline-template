# Javascript template
testing template for Javascript application

# Installation
to use this template you need to: 

- Copy the `./test` folder into the project root `./tests` folder.
- Adjust `package.json` file (`package.json.example` for inspiration)
	- `scripts`: for tests
	- `devDependencies`: packages needed for testing (`jest`, ...)
- Install required packages in the project `Dockerfile`
	- `npm` and `nodejs` for unit and integration tests

# Usage
The only thing left is to implement application specific tests by 
changing `tests/unit-test/*`, `tests/end-to-end-test/*`, `tests/integration-test/*`
